## Overview

This repository is an example for how to use the `kaniko-component` from `gitlab.com/aaron-prindle/kaniko-component` in a GitLab CI/CD pipeline. The `.gitlab-ci.yaml` here shows how to integrate the Kaniko-based Docker image building process into your GitLab projects.

## Purpose

This repository is set up to provide a practical example of incorporating the Kaniko component for Docker image building in a GitLab CI/CD pipeline. It shows how to reference and use the Kaniko component in a real-world scenario.

Last updated: 2/5/2024